<?php
/**
 * Created by Vim.
 * User: test
 * Date: 18-3-11
 * Time: 上午11:16
 */

namespace HelloWorld;

class HelloWorld
{
    protected $author;

    /**
     * HelloWorld constructor.
     * @param string $author
     */
    public function __construct($author = 'John')
    {
        $this->author = $author;
    }

    /**
     * print HelloWorld
     * @return string
     */
    public function info()
    {
        $info = "Hello World ! \n";
        $info .= "\t--Power By ";
        $info .= $this->author . "\n";
        return $info;
    }
}
